# 2.45GHz中心频率FR4板材微带带通滤波器（平行耦合线结构）

## 简介

本仓库提供了一个中心频率为2.45GHz的微带带通滤波器设计资源。该滤波器采用平行耦合线结构，适用于FR4板材（介电常数为4.4，损耗角正切为0.02，介质板厚度为1mm）。设计文件使用ANSYS HFSS 2021 R2版本进行仿真和优化。

## 资源内容

- **滤波器设计文件**：包含HFSS项目文件，可直接在ANSYS HFSS 2021 R2中打开并查看设计细节。
- **仿真结果**：提供了滤波器的S参数、频率响应等仿真结果，帮助用户快速了解滤波器的性能。
- **设计文档**：详细的设计说明文档，包括设计思路、参数选择、仿真步骤等，方便用户理解和复现设计。

## 使用说明

1. **软件要求**：确保您已安装ANSYS HFSS 2021 R2或更高版本。
2. **打开项目**：下载并解压仓库中的文件，使用HFSS打开项目文件。
3. **查看设计**：在HFSS中查看滤波器的设计模型、参数设置和仿真结果。
4. **仿真与优化**：根据需要，您可以对设计进行进一步的仿真和优化，以满足特定的应用需求。

## 贡献与反馈

如果您在使用过程中有任何问题或建议，欢迎通过GitHub的Issues功能提出。我们也非常欢迎您提交改进建议或新的设计方案，共同完善这个资源库。

## 许可证

本项目采用MIT许可证，您可以自由使用、修改和分发本项目中的资源，但请遵守许可证中的相关条款。

---

希望这个资源对您的设计工作有所帮助！